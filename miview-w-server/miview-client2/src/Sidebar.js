
import React, { Component } from 'react';
import Feedback from './Feedback';


class Sidebar extends Component {
  render(){
    return(

        <ul class="sidebar navbar-nav">
          <li class="nav-item active">
            <a class="nav-link" href="home.html">
              <i class="fas fa-fw fa-chart-area"></i>
              <span>Home</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="index.html">
              <i class="fas fa-fw fa-tachometer-alt"></i>
              <span>Lost and Found</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="Feedback.js">
              <Feedback/>
              <i class="fas fa-fw fa-tachometer-alt"></i>
              <span>Feedback</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="Cohort.js">
              <i class="fas fa-fw fa-tachometer-alt"></i>
              <span>Cohort</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="broadcast.html">
              <i class="fas fa-fw fa-tachometer-alt"></i>
              <span>Broadcast</span>
            </a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i class="fas fa-fw fa-folder"></i>
              <span>Pages</span>
            </a>
            <div class="dropdown-menu" aria-labelledby="pagesDropdown">
              <h6 class="dropdown-header">Login Screens:</h6>
              <a class="dropdown-item" href="login.html">Login</a>
              <a class="dropdown-item" href="register.html">Register</a>
              <a class="dropdown-item" href="forgot-password.html">Forgot Password</a>
              <div class="dropdown-divider"></div>
              <h6 class="dropdown-header">Other Pages:</h6>
              <a class="dropdown-item" href="404.html">404 Page</a>
              <a class="dropdown-item" href="blank.html">Blank Page</a>
            </div>
          </li>
          <li class="nav-item active">
            <a class="nav-link" href="tables.html">
              <i class="fas fa-fw fa-table"></i>
              <span>Chartr</span></a>
          </li>
        </ul>


    );
  }
}
export default Sidebar;
