// import React, { Component } from 'react';
// import Sidebar from './Sidebar';

// import {Doughnut} from 'react-chartjs-2'



// class Feedback extends Component {
//   render(){
//     return(
//       <div>
//         {/*Can be removed; temp initial and header */}
//         <Initial/>
//         <Header/>

//           {/*Don't touch*/}
//         <FeedbackCon/>


//       </div>
//     );
//   }
// }


// class FeedbackCon extends Component {
//   render(){
//     return(
//         <div id="wrapper">
//             {/*For easy way of calling sidebar and putting it together with the cohort display*/}
//           <Sidebar/>
//           <FeedbackChart/>
//       </div>
//     );
//   }
// }

// // Temporary; can be removed when actual header and sidear are available

// class Initial  extends Component {
//   render(){
//     return(
//       <div>
//         <meta charset="utf-8"/>
//         <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
//         <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
//         <meta name="description" content=""/>
//         <meta name="author" content=""/>

//         <title>SB Admin - Tables</title>

//         <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet"/>

//         <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css"/>

//         <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet"/>

//         <link href="css/sb-admin.css" rel="stylesheet"/>

//       </div>
//     );
//   }
// }


// class Header extends Component {
//   render(){
//     return(
//       <div class="navbar navbar-expand navbar-dark bg-dark static-top">

//           <a class="navbar-brand mr-1" href="index.html">miView</a>

//           <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#">
//             <i class="fas fa-bars"></i>
//           </button>


//           <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
//             <div class="input-group">
//               <input type="text" class="form-control" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2"/>
//               <div class="input-group-append">
//                 <button class="btn btn-primary" type="button">
//                   <i class="fas fa-search"></i>
//                 </button>
//               </div>
//             </div>
//           </form>




//       </div>
//     );
//   }
// }

// //ends here temp


// // Cohort code. Don't touch
// class FeedbackChart extends Component {
//   constructor(props){
//   super(props);
//   this.state = {
//       chartData:{
//       labels: ['Good','Bad','Neutral'],
//       datasets:[
//         {
//           label:'Feedback of Drivers',
//           data:[
//             190,
//             45,
//             253
//           ],
//           backgroundColor:[
//            "#fec34d",
//             "#ff8106",
//             "#e55300"
//           ]
//         }
//       ]
//     },

//   };

// }



// render(){
//   return (
//     <div>
//       <br/>
//       <Doughnut
//         width={1000}
// 	      height={400}
//         data={this.state.chartData}
//         options={{

//           backgroundColor: 'rgba(251, 85, 85, 0.4)',

//           title:{
//             display:true,
//             text:"Drivers under Cohort Type",
//             fontSize:25
//           },
//           legend:{
//             display:false
//           },
//           onClick: alertBox(this.state.chartData.labels),
//         }}
//       />
//     <Inside/>
//     </div>

//   )
// }
// }
// function alertBox(item){

//   console.log("Clicked " +item);


// }


// class Inside extends Component {
//   render(){
//     return(
//       <div id="content-wrapper">
//         <div class="container-fluid">
//           <div class="card mb-3">
//             <div class="card-header">

//               Driver List</div>
//             <div class="card-body">
//               <div class="table-responsive">
//                 <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
//                   <thead>
//                     <tr>
//                       <th>Device #</th>
//                       <th>Driver Name</th>
//                       <th>Driver Liscense</th>
//                       <th>Organization</th>
//                       <th>Unit Plate</th>
//                       <th>Region</th>
//                       <th>Days Offline</th>
//                       <th>Cohort</th>
//                     </tr>
//                   </thead>
//                   <tbody>
//                     <tr>
//                       <td>123456</td>
//                       <td>Lorem</td>
//                       <td>67281</td>
//                       <td>Ipsum</td>
//                       <td>DET567</td>
//                       <td>Cebu</td>
//                       <td>2</td>
//                       <td>A</td>
//                     </tr>
//                     <tr>
//                       <td>123456</td>
//                       <td>Lorem</td>
//                       <td>67281</td>
//                       <td>Ipsum</td>
//                       <td>DET567</td>
//                       <td>Cebu</td>
//                       <td>2</td>
//                       <td>A</td>
//                     </tr>
//                     <tr>
//                       <td>123456</td>
//                       <td>Lorem</td>
//                       <td>67281</td>
//                       <td>Ipsum</td>
//                       <td>DET567</td>
//                       <td>Cebu</td>
//                       <td>2</td>
//                       <td>A</td>
//                     </tr>
//                     <tr>
//                       <td>23456</td>
//                       <td>Lorem</td>
//                       <td>67281</td>
//                       <td>Ipsum</td>
//                       <td>DET567</td>
//                       <td>Cebu</td>
//                       <td>2</td>
//                       <td>A</td>
//                     </tr>
//                     <tr>
//                       <td>123456</td>
//                       <td>Lorem</td>
//                       <td>67281</td>
//                       <td>Ipsum</td>
//                       <td>DET567</td>
//                       <td>Cebu</td>
//                       <td>2</td>
//                       <td>A</td>
//                     </tr>

//                   </tbody>

//                 </table>
//               </div>
//             </div>
//             <div class="card-footer small text-muted"></div>
//           </div>
//         </div>
//       </div>
//     );
//   }
// }



//  export default Feedback;
