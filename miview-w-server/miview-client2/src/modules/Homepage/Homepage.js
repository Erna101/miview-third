import React from "react";
import { Link } from "react-router-dom";

const Homepage = () =>(
    <div className="home-hero">
        <h1 className="welcome-text">Welcome to miView</h1>
    </div> 
);

export default Homepage;