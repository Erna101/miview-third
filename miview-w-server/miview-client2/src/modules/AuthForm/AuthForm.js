import React,{ Component } from "react";

class AuthForm extends Component{
    constructor(props){
        super(props);
        this.state = {
            email: "",
            username: "",
            password: "",
            profileImageUrl: ""
        };
    }

    handleChange = (e) =>{
        this.setState({
            [e.target.name]: e.target.value
        });
    };

    handleSubmit = (e) =>{
        e.preventDefault();
        const authType = this.props.signup ? "signup" : "signin";
        this.props.onAuth(authType, this.state).then(() => {
            console.log("S U C C E S S!");
        });
    };

    render(){
        const { email, username, password, profileImageUrl } = this.state;
        const { heading, buttonText, signup} = this.props;
        return(
            <div>
                <div className="container">
                <div className="card card-login mx-auto mt-5">
                    <div className="card-header"><h4 className="comfortaa-text">{heading}</h4></div>
                    <div className="card-body">
                    <form onSubmit={this.handleSubmit}>
                        <div className="form-group">
                        <div className="form-label-group">
                            <input type="email" id="inputEmail" onChange={this.handleChange} name ="email" value={email} className="form-control" placeholder="Email address" required="required" autoFocus="autofocus"/>
                            <label htmlFor="inputEmail">Email address</label>
                        </div>
                        </div>
                        <div className="form-group">
                        <div className="form-label-group">
                            <input type="password" id="inputPassword" name="password" onChange={this.handleChange} className="form-control" placeholder="Password" required="required"/>
                            <label htmlFor="inputPassword">Password</label>
                        </div>
                        </div>
                        {signup && (
                            <div>
                                <div className="form-group">
                                <div className="form-label-group">
                                    <input type="text" id="username" onChange={this.handleChange} name ="username" value={username} className="form-control" placeholder="Username" required="required" autoFocus="autofocus"/>
                                    <label htmlFor="username">Username</label>
                                </div>
                                </div>
                                <div className="form-group">
                                <div className="form-label-group">
                                    <input type="text" id="image-url" name="profileImageUrl" onChange={this.handleChange} className="form-control" placeholder="Profile Image"/>
                                    <label htmlFor="image-url">Profile Image</label>
                                </div>
                                </div>
                            </div>
                        )}
                        <div className="form-group">
                        <div className="checkbox">
                            <label>
                            <input type="checkbox" value="remember-me"/>
                            Remember Password
                            </label>
                        </div>
                        </div>
                        <button type="submit" className="btn btn-primary btn-block">{buttonText}</button>
                    </form>
                    </div>
                </div>
                </div>
                <script src="vendor/jquery/jquery.min.js"></script>
                <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
                <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
            </div>
        );
    }
}

export default AuthForm;