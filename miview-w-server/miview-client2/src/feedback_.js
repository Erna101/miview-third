Chart.defaults.global.defaultFontFamily =
  '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
Chart.defaults.global.defaultFontColor = "#292b2c";

// Pie Chart Example
var ctx = document.getElementById("myPieChart");
var myPieChart = new Chart(ctx, {
  type: "Doughnut",
  data: {
    labels: ["Good", "Bad", "Neutral"],
    datasets: [
      {
        data: [12.21, 15.58, 11.25],
        backgroundColor: ["#28a745", "#dc3545", "#ffc107"]
      }
    ]
  }
});

/* export class FeedbackSidebar extends Component {
  render() {
    return (
      <li class="nav-item active">
        <a class="nav-link" href="feedback_index.html">
          <i class="fas fa-fw fa-table" />
          <span>Feedback</span>
        </a>
      </li>
    );
  }
}

export default Feedback;
 */
