// Set new default font family and font color to mimic Bootstrap's default styling
Chart.defaults.global.defaultFontFamily =
  '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
Chart.defaults.global.defaultFontColor = "#292b2c";

// Bar Chart Example
var ctx = document.getElementById("myBarChart");
var myLineChart = new Chart(ctx, {
  type: "bar",
  data: {
    labels: ["A", "B", "C"],
    datasets: [
      {
        label: "Cohort",
        backgroundColor: ["#28a745", "#dc3545", "#ffc107"],
        data: [4215, 5312, 6251]
      }
    ]
  },
  options: {
    scales: {
      xAxes: [
        {
          time: {
            unit: "number"
          },
          gridLines: {
            display: false
          },
          ticks: {
            maxTicksLimit: 6
          }
        }
      ],
      yAxes: [
        {
          ticks: {
            min: 0,
            max: 15000,
            maxTicksLimit: 5
          },
          gridLines: {
            display: true
          }
        }
      ]
    },
    legend: {
      display: false
    }
  }
});
