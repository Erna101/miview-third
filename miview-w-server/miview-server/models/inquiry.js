const mongoose = require("mongoose");
const User = require('./user');

const inquirySchema = ({
    user:{
        type: mongoose.Schema.Types.ObjectId,
        ref: User
    },
    name:{
        type: String,
        required: true,
        maxLength: 160
    },
    date:{
        type: Date,
        required: true
    },
    location:{
        type: String,
        required: true,
        maxLength: 160
    },
    operator:{
        type: String,
        required: true,
        maxLength: 160
    },
    belonging:{
        type: String,
        required: true,
    },
    note:{
        type: String
    }


});

const Inquiry = mongoose.model('Inquiry', inquirySchema);
module.exports = Inquiry;